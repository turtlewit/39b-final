BUFFER_X = 80
def LineConvert(line):
    lineBeginsWithNewLine = False
    if line[0] == "\n":
        lineBeginsWithNewLine = True
    line = line.split()
    linelen = 0
    if len(line) > 0:
        word = line.pop(0)
    else:
        word = ""
    wordsplit = list(word)
    linelen += len(wordsplit) + 1
    newLine = word
    for x in range(0, len(line)):
        word = line.pop(0)
        wordsplit = list(word)
        linelen += len(wordsplit) + 1
        if linelen > BUFFER_X - 2:
            newLine = "%s\n" % (newLine)
            linelen = 0
            newLine = "%s%s" % (newLine, word)
            linelen += len(wordsplit) + 1
        else:
            newLine = "%s %s" % (newLine, word)
    if lineBeginsWithNewLine:
        newLine = "\n%s" % (newLine)
    return newLine

class TextCommand:
    def __init__(self, text, commandInputs):
        self.text = text
        self.commandInputs = commandInputs

class MoveCommand:
    def __init__(self, targetRoomIndex, commandInputs):
        self.targetRoom = targetRoomIndex
        self.commandInputs = commandInputs

class Room:
    def __init__(self, description, textCommands, moveCommands, specialCommands=[]):
        self.description = description
        self.textCommands = textCommands
        self.moveCommands = moveCommands
        self.specialCommands = specialCommands

    def action(self, game):
        pass

class LectureHall(Room):
    def __init__(self, description, textCommands, moveCommands, specialCommands=[]):
        Room.__init__(self, description, textCommands, moveCommands, specialCommands)

    def action(self, game):
        if not game.player.hasEnteredLectureHall:
            game.player.hasEnteredLectureHall = True
            print(LineConvert("\nSeveral minutes after you take your seat, your professor arrives and starts the class.\n\"Welcome, class. Today is your first day here at the School of Magic in Bavaria. Your first assignment is to make a spell. It must create a green flash of light. You will be graded on the accuracy of the spell to this premise. You can check the Library for references, and you can make the spell in the Lab. Class dismissed.\""))

class DormRoom(Room):
    def __init__(self, description, textCommands, moveCommands, specialCommands=[]):
        Room.__init__(self, description, textCommands, moveCommands, specialCommands)

    def action(self, game):
        if game.player.assignmentTurnedIn == True:
            print(LineConvert("\nGame Over! Thanks for playing!\n\n"))
            print("")
            raise SystemExit

class SpecialCommand:
    def __init__(self, game, commandInputs):
        self.game = game
        self.commandInputs = commandInputs

    def action(self):
        pass

class LookThroughSpellTomes(SpecialCommand):
    def __init__(self, game, commandInputs):
        SpecialCommand.__init__(self, game, commandInputs)

    def action(self):
        print(LineConvert("\nYou look through one of the many shelves in search of some books you can use to help you build your spell. Several titles catch your eye, such as: To Catch a Mockingbird: Spells for catching Swan Maidens, Spells that Kill People by Bluebeard, A Wolf’s Tome of Illusion Spells, and so on. You decide on two spell tomes that seem useful: Eulenspiegel’s Party Tricks, which includes a spell for a flash of white light, and unmarked tome for casting green-colored explosions."))
        self.game.player.libraryTome = 1
        
class LookThroughSourceTomes(SpecialCommand):
    def __init__(self, game, commandInputs):
        SpecialCommand.__init__(self, game, commandInputs)

    def action(self):
        print(LineConvert("\nYou look through one of the many shelves in search of some books you can use to help you build your spell. Several titles catch your eye, such as: Little Red Riding Hood’s Counter-Illusory Spells, Belle’s Beast-Taming Tome, Spells to Find Your Way Home by Hansel & Gretel, and Rapunzel’s Hair Care Spell Reference. You pick out two that might be useful for crafting your spell: A Study of Light and Properties of Color in Spells, both community-maintained reference books."))
        self.game.player.libraryTome = 2

class CreateTome(SpecialCommand):
    def __init__(self, game, commandInputs):
        SpecialCommand.__init__(self, game, commandInputs)

    def action(self):
        if self.game.player.libraryTome == 0:
            print(LineConvert("\nYou don't know how to make spells. Try hitting up the library."))
        elif self.game.player.libraryTome == 1:
            self.game.player.playerTome = 1
            print(LineConvert("\nYou get to work combining the nonsense words of your two spell tomes. You don’t really know what you’re doing, but you get it to look somewhat like a real spell. You now have your own spell tome!"))
        elif self.game.player.libraryTome == 2:
            self.game.player.playerTome = 2
            print(LineConvert("\nYou get to work studying your two reference tomes. You learn the basics of how to craft a light spell, and you modify it using what you learned of color in the other tome. You then spend a while translating what you have into a usable form. You now have your own source and spell tome!"))

class TestSpell(SpecialCommand):
    def __init__(self, game, commandInputs):
        SpecialCommand.__init__(self, game, commandInputs)

    def action(self):
        if self.game.player.playerTome == 0:
            print(LineConvert("\nYou don’t have any spells to test!"))
        elif self.game.player.playerTome == 1:
            print(LineConvert("\nYou ready your new spell tome, and read out the words. When you finish, sudden cracks of green and white sparks fly about in front of you. Not exactly what you were going for, but it’ll get you a grade."))
        elif self.game.player.playerTome == 2:
            print(LineConvert("\nYou ready your new spell tome, and read out the words. When you finish, your vision is filled with green, and slowly fades back to normal. Perfect!"))

class TurnInSpell(SpecialCommand):
    def __init__(self, game, commandInputs):
        SpecialCommand.__init__(self, game, commandInputs)

    def action(self):
        if self.game.player.assignmentTurnedIn:
            print(LineConveert("\nYou've already turned in your assignment!"))
        else:
            if self.game.player.playerTome == 0:
                print(LineConvert("\nYou don't have anything to turn in!"))
            elif self.game.player.playerTome == 1:
                self.game.player.assignmentTurnedIn = True
                print(LineConvert("\nYou approach the professor, ready to present your new spell. She asks you to demonstrate the spell, and you prepare the tome. You cast the spell, and sudden cracks of green and white sparks fly about in front of you. \"Not bad, but it could be better,\" says your professor. You are given a passing grade, and return to your seat.\nAfter everyone presents their spells, your professor says one last thing.\n\"Now that you have all presented your spells, you are free to do what you wish with them. You can sell the spell tome to the library, or perhaps donate the source. But this is not mandatory, and you can just return to your dorm room if you wish.\""))
            elif self.game.player.playerTome == 2:
                self.game.player.assignmentTurnedIn = True
                print(LineConvert("\nYou approach the professor, ready to present your new spell. She asks you to demonstrate the spell, and you prepare the tome. You cast the spell, and your vision is filled with green, and slowly fades back to normal. \"Excellent!\" says your professor. You are given a perfect score, and return to your seat.\nAfter everyone presents their spells, your professor says one last thing.\n\"Now that you have all presented your spells, you are free to do what you wish with them. You can sell the spell tome to the library, or perhaps donate the source. But this is not mandatory, and you can just return to your dorm room if you wish.\""))

class SellBooks(SpecialCommand):
    def __init__(self, game, commandInputs):
        SpecialCommand.__init__(self, game, commandInputs)

    def action(self):
        print(LineConvert("\nYou approach the counter, and a old man asks \"Would you like to sell or donate today?\"\n(sell, donate, leave)"))
        playerInput = input(">")
        if playerInput.lower() == "leave":
            pass
        else:
            if playerInput.lower() == "sell":
                if self.game.player.playerTome == (0 or 2):
                    print(LineConvert("You don't have any tomes to sell!"))
                else:
                    print(LineConvert("You hand over your tome and the attendant gives you a bag of gold."))
            if playerInput.lower() == "donate":
                if self.game.player.playerTome == (0 or 1):
                    print(LineConvert("You don't have any tomes to donate!"))
                else:
                    print(LineConvert("You hand over  your tome and the attendant thanks you for your donation."))

class Player:
    def __init__(self, startRoom):
        self.room = startRoom
        self.hasEnteredLectureHall = False
        self.libraryTome = 0
        self.playerTome = 0
        self.assignmentTurnedIn = False

class SystemCommand:
    def __init__(self, commandInputs):
        self.commandInputs = commandInputs

    def action(self, game):
        pass

class QuitCommand(SystemCommand):
    def __init__(self, commandInputs):
        SystemCommand.__init__(self, commandInputs)

    def action(self, game):
        game.gameRunning = False

class Game:
    def __init__(self):
        self.gameRunning = True
        self.rooms = []

        #Bed (ID 0)
        bedDesc = "You wake up in your dorm room. You attend the School of Magic in Bavaria, and it is the first day of classes. You need to make your way to class at the Lecture Hall to receive your first assignment."

        bedMoveCommandTargetRoom = 1
        bedMoveCommandInput0 = "climb out of bed"
        bedMoveCommandInputs = [bedMoveCommandInput0, "get out of bed"]
        bedMoveCommand = MoveCommand(bedMoveCommandTargetRoom, bedMoveCommandInputs)
        bedMoveCommands = [bedMoveCommand]

        bedRoom = Room(bedDesc, [], bedMoveCommands)
        self.rooms.append(bedRoom)

        #Dorm room (ID 1)
        dormDesc = "You are in your dorm room. You live with your roommate, who is not currently here. Your bed floats above his in a magical bunk-bed configuration, with a short ladder to climb in and out. Thankfully, you don’t get motion sickness.\nThere are two writing desks on the far wall, the one closest to the window being yours. There is a collection of books and scrolls that you brought with you from home, as well as a quill and a bottle of ink, sitting on your desk.\nThe door out of the room is to the east."

        dormTextCommandBedText = "As tempting as the thought is, you really must get to class. It’s your first day!"
        dormTextCommandBedCommandInput0 = "climb into bed"
        dormTextCommandBedCommandInputs = [dormTextCommandBedCommandInput0]
        dormTextCommandBed = TextCommand(dormTextCommandBedText, dormTextCommandBedCommandInputs)

        dormTextCommandWindowText = "Your windows presents a view of the school grounds, specifically the Quadradaplatz Square. There are many students walking around between the various places in the school."
        dormTextCommandWindowCommandInput0 = "examine window"
        dormTextCommandWindowCommandInput1 = "look at window"
        dormTextCommandWindowCommandInputs = [dormTextCommandWindowCommandInput0, dormTextCommandWindowCommandInput1]
        dormTextCommandWindow = TextCommand(dormTextCommandWindowText, dormTextCommandWindowCommandInputs)

        dormTextCommands = [dormTextCommandBed, dormTextCommandWindow]

        dormMoveCommandTargetRoom = 2
        dormMoveCommandInputs = ["east", "e"]
        dormMoveCommand = MoveCommand(dormMoveCommandTargetRoom, dormMoveCommandInputs)
        dormMoveCommands = [dormMoveCommand]

        dormRoom = DormRoom(dormDesc, dormTextCommands, dormMoveCommands)
        self.rooms.append(dormRoom)

        #Common Room (ID 2)
        crD = "You are in the common room. There are various chairs and tables about, with a few people sitting over a book. There are several posters around the room detailing fun facts about the university that you’ve heard several times before.\nThe exit to the square is to the north, and your room to the west."

        crTCSText = "None of them seem too keen to talk right now, as many are studying for the assignment you’re running late to get."
        crTCSCIs = ["talk to students"]
        crTCS = TextCommand(crTCSText, crTCSCIs)
        crTCs = [crTCS]

        crMCDTR = 1
        crMCDCIs = ["west", "dorm room", "into dorm room"]
        crMCD = MoveCommand(crMCDTR, crMCDCIs)

        crMCSTR = 3
        crMCSCIs = ["north", "square", "to the square"]
        crMCS = MoveCommand(crMCSTR, crMCSCIs)

        crMCs = [crMCD, crMCS]

        crR = Room(crD, crTCs, crMCs)
        self.rooms.append(crR)

        #Square (ID 3)
        sD = "You are in the Quadradplatz Square: a large, open area with grass, trees, and stone paths leading to different places within the school. You see many of your fellow students attempting to cast spells with varying degrees of success. With the exception of the occasional spark or burst of light, it is quite calm here in the square.\nThere is a path laid with circular stones leading north to the Lecture Hall. There is a gravel path connecting the east and west wings, which house the Library and the Lab respectively. There is a network of smaller, interconnecting paths that connect the various Common Rooms to the south."

        sTCs = []

        sMCSTR = 2
        sMCSCIs = ["south", "common room", "to common room"]
        sMCS = MoveCommand(sMCSTR, sMCSCIs)

        sMCNTR = 4
        sMCNCIs = ["north", "class room", "classroom", "to class room", "to classroom", "lecture hall", "to the lecture hall", "to lecture hall"]
        sMCN = MoveCommand(sMCNTR, sMCNCIs)

        sMCETR = 5
        sMCECIs = ["east", "library", "to the library"]
        sMCE = MoveCommand(sMCETR, sMCECIs)

        sMCWTR = 7
        sMCWCIs = ["west", "lab", "to the lab"]
        sMCW = MoveCommand(sMCWTR, sMCWCIs)

        sMCs = [sMCS, sMCN, sMCE, sMCW]
        
        sR = Room(sD, sTCs, sMCs)
        self.rooms.append(sR)

        #Lecture Hall (ID 4)
        lD = "You are in the Classroom. There are rows of seats that slope downward towards the lecture podium. The professor sits at a desk on this podium, sifting through papers and scrolls. There are students scattered around the room immersed in various books and scrolls.\nThe exits are to the south."

        lTCs = []

        lMCTR = 3
        lMCCIs = ["south", "leave"]
        lMC = MoveCommand(lMCTR, lMCCIs)

        lMCs = [lMC]

        lSCCIs = ["turn in assignment", "turn in spell", "turn in"]
        lSC = TurnInSpell(self, lSCCIs)
        
        lSCs = [lSC]
        
        lR = LectureHall(lD, lTCs, lMCs, lSCs)
        self.rooms.append(lR)

        #Library (ID 5)
        zD = "You are in the great Zauberwulf Library, dedicated to the great mage Gildenstern Zauberwulf. It is a great, circular building with several floors. Books line the circumference, and there are several other shelves scattered about. There is a staircase in the middle of the room leading up to the second floor.\nThis floor is the floor for buying licensed spell tomes. These tomes contain finished spells you can use, but are written in a form nobody can really understand. There are several flashy displays for various types of spells you can use for your assignments.\nThere is a counter near the front of the room with a sign saying “Tome Sale / Donation”.\nThe great, arched entrance is to the west."

        zTCs = []

        zMCUTR = 6
        zMCUCIs = ["up", "up the stairs", "upwards", "stairs"]
        zMCU = MoveCommand(zMCUTR, zMCUCIs)

        zMCETR = 3
        zMCECIs = ["west", "to the square", "square", "leave"]
        zMCE = MoveCommand(zMCETR, zMCECIs)

        zMCs = [zMCU, zMCE]

        zSCCIs = ["look through books", "look for books", "seach for books", "search books", "search bookshelves"]
        zSC = LookThroughSpellTomes(self, zSCCIs)

        zSCSCIs = ["sell / donate tomes", "sell tome", "donate tome", "sell tomes", "donate tomes"]
        zSCS = SellBooks(self, zSCSCIs)

        zSCs = [zSC, zSCS]

        zR = Room(zD, zTCs, zMCs, zSCs)
        self.rooms.append(zR)

        #Library Floor 2 (ID 6)
        wD = "You are in the second floor of the library. Much like the first floor, books line the walls, and there are other bookshelves about the place. Unlike the first floor, there are no flashy displays for selling you books. The only signs present are for organization. That is because this is the Source Tomes section of the library, where there are books you can use to write the source material for your spell. These tomes aren’t translated so you can use different parts of various books to make exactly what you want."

        wTCs = []

        wMCTR = 5
        wMCCIs = ["down", "stairs", "down the stairs", "leave"]
        wMC = MoveCommand(wMCTR, wMCCIs)

        wMCs = [wMC]

        wSC = LookThroughSourceTomes(self, zSCCIs)
        
        wSCs = [wSC]

        wR = Room(wD, wTCs, wMCs, wSCs)
        self.rooms.append(wR)

        #Lab (ID 7)
        bD = "You are in the Lab. Here, students work on their latest spells, and then test them. There are writing desks against the west wall, where students can write and translate their spells. In the north of the room, there are various devices used to aid the practice and testing of spells."

        bTCs = []

        bMCTR = 3
        bMCCIs = ["east", "square", "to the square", "leave"]
        bMC = MoveCommand(bMCTR, bMCCIs)

        bMCs = [bMC]

        bSCCCIs = ["create spell", "create tome", "make spell", "make tome"]
        bSCC = CreateTome(self, bSCCCIs)

        bSCTCIs = ["test spell", "cast spell", "test tome"]
        bSCT = TestSpell(self, bSCTCIs)

        bSCs = [bSCC, bSCT]

        bR = Room(bD, bTCs, bMCs, bSCs)
        self.rooms.append(bR)

        self.player = Player(self.rooms[0])
        self.movementVerbs = ["go ", "move ", ""]
        self.movemendVerbsMinusEmpty = ["go ", "move "]
        self.systemCommands = []

        quitCommandInputs = ["quit", "exit", "escape"]
        quitCommand = QuitCommand(quitCommandInputs)
        self.systemCommands.append(quitCommand)

        print(LineConvert(self.player.room.description))
        commands = []
        for textCommand in self.player.room.textCommands:
            if textCommand.commandInputs != []:
                commands.append(textCommand.commandInputs[0])
        for movementCommand in self.player.room.moveCommands:
            if movementCommand.commandInputs != []:
                commands.append("%s" % (movementCommand.commandInputs[0]))
        commandString = "("
        for command in commands:
            commandString += "%s" % (command)
        commandString += ")"
        print(commandString)

    def checkTextCommands(self, playerInput):
        for textCommand in self.player.room.textCommands:
            for commandInput in textCommand.commandInputs:
                if playerInput.lower() == commandInput.lower():
                    print(LineConvert("\n%s" % (textCommand.text)))

    def checkMoveCommands(self, playerInput):
        moveAttempt = False
        moveSuccess = False
        for moveCommand in self.player.room.moveCommands:
            for commandInput in moveCommand.commandInputs:
                for movementVerb in self.movementVerbs:
                    if movementVerb.lower() in playerInput.lower():
                        if movementVerb.lower() != "":
                            moveAttempt = True
                    if playerInput.lower() == "%s%s" % (movementVerb.lower(), commandInput.lower()):
                        self.player.room = self.rooms[moveCommand.targetRoom]
                        self.player.room.action(self)
                        print(LineConvert("\n%s" % (self.player.room.description)))
                        commands = []
                        for textCommand in self.player.room.textCommands:
                            if textCommand.commandInputs != []:
                                commands.append(textCommand.commandInputs[0])
                        for movementCommand in self.player.room.moveCommands:
                            if movementCommand.commandInputs != []:
                                commands.append("%s%s" % (self.movementVerbs[0], movementCommand.commandInputs[0]))
                        for specialCommand in self.player.room.specialCommands:
                            if specialCommand.commandInputs != []:
                                commands.append(specialCommand.commandInputs[0])
                        commandString = "("
                        for command in commands:
                            commandString += "%s, " % (command)
                        commandString = commandString[:-2] #lol
                        commandString += ")"
                        print(commandString)
                        moveSuccess = True
        if moveAttempt and not moveSuccess:
            print("\nYou cannot go that way.")

    def checkSystemCommands(self, playerInput):
        for systemCommand in self.systemCommands:
            for commandInput in systemCommand.commandInputs:
                if playerInput.lower() == commandInput.lower():
                    systemCommand.action(self)

    def checkSpecialCommands(self, playerInput):
        for specialCommand in self.player.room.specialCommands:
            for commandInput in specialCommand.commandInputs:
                if playerInput.lower() == commandInput.lower():
                    specialCommand.action()

    def loop(self):
        playerInput = input(">")

        self.checkSpecialCommands(playerInput)
        self.checkTextCommands(playerInput)
        self.checkMoveCommands(playerInput)
        self.checkSystemCommands(playerInput)



def main():
    game = Game()

    while game.gameRunning == True:
        game.loop()

if __name__ == "__main__":
    main()
